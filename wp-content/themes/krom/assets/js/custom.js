$ = jQuery;

$(".header .icon-search").on("click", function () {
  $(".searchbar").toggleClass("active");
});

// Sticky Header Start
$(window).scroll(function () {
  var baseurl = 'http://localhost/krom';
  var fromTopPx = 200;
  var scrolledFromtop = $(window).scrollTop();
  if (scrolledFromtop > fromTopPx) {
    $("header.header").addClass("fixed");
    $("body").addClass("header-fixed");
    $("navbar-nav").addClass("header-fixed");
    $(".header.header img").attr(
      "src",
      baseurl + "/wp-content/uploads/2022/08/ic_krom_logo.png",
    );
  } else {
    $("header.header").removeClass("fixed");
    $("body").removeClass("header-fixed");
    $(".header.header img").attr(
      "src",
      baseurl + "/wp-content/uploads/2022/08/cropped-Logo-Full-Krom-Putih.png",
    );
  }
});

// Sticky Header End

$(".home-slider").owlCarousel({
  loop: true,
  margin: 0,
  nav: false,
  autoplay: true,
  autoplayTimeout: 5000,
  responsive: {
    0: {
      items: 1,
    },
  },
});
