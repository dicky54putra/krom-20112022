<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package krom
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="facebook-domain-verification" content="vpyld7qz43hdwsfm5xwd26p098se6j" />
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/carousel/">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<!-- Pas nge klik faq ada bg text -->
	<script>
		var body = document.querySelector('body');
		body.style.display = 'none'
		document.addEventListener('readystatechange', event => {
			switch (document.readyState) {
				case "loading":
					console.log("document.readyState: ", document.readyState,
						`- The document is still loading.`
					);
					break;
				case "interactive":
					console.log("document.readyState: ", document.readyState,
						`- The document has finished loading DOM. `,
						`- "DOMContentLoaded" event`
					);
					break;
				case "complete":
					body.style.display = 'block'
					console.log("document.readyState: ", document.readyState,
						`- The page DOM with Sub-resources are now fully loaded. `,
						`- "load" event`
					);

					break;
			}
		});
	</script>
	<?php wp_body_open(); ?>
	<div id="page" class="site">
		<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e('Skip to content', 'krom'); ?></a>
		<!-- add controller to set in the mobile view -->
		<header class="header vc-header">
			<div class="container">
				<div class="header-wrap d-flex flex-column flex-md-row align-items-center">
					<div class="header-logo">
						<?php the_custom_logo(); ?>
					</div>
					<div class="header">
						<div class="header-bottom my-0 mr-md-auto">
							<div class="container-fluid">
								<nav class="navbar navbar-expand-lg navbar-light">
									<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
										<span class="navbar-toggler-icon">
											<span></span>
											<span></span>
											<span></span>
										</span>
										<span class="menu-text"><?php _e('', 'krom'); ?></span>
									</button>

									<div class="collapse navbar-collapse" id="navbarSupportedContent">
										<?php
										wp_nav_menu(array(
											'theme_location'    => 'menu-primary',
											'list_item_class'   => "nav-item",
											'link_class'        => "nav-link",
											'depth'             => 3,
											'container'         => '',
											'menu_class'        => 'navbar-nav',
											'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
											'walker'            => new WP_Bootstrap_Navwalker()
										));
										?>
									</div>
							</div>
							</nav>
						</div>
					</div>
					<div class="col-8 d-flex justify-content-end align-items-center wrap-btn-download">
						<button type="button" class="btn btn-primary btn-download btn-download-header" data-toggle="modal" data-target="#download-Modal">
							Download
						</button>
					</div>
				</div>
			</div>


		</header>
		<!-- Header End -->