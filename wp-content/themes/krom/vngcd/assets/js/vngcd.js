(function ($) {
  $(document).ready(function () {
    function init() {
      handleClick();
      homeSectionThree();
      homeSectionFour();
      faq();
      if (window.screen.width > 600) {
        faqAccordion();
      }
      addXsearch();
    }

    var el = {
      accordion: ".js-accordion-home-section-five",
      btn: ".js-btn-acc",
      img: ".js-home-section-five-image",
    };

    function handleClick() {
      if ($(el.accordion).length === 0) return;
      $(el.accordion).on("click", el.btn, function () {
        if (!$(this).hasClass("hide")) {
          $(el.btn).removeClass("hide");
          $(this).addClass("hide");
        }
        $(el.img).attr("src", $(this).data("img"));
      });
    }

    function faq() {
      var elFaq = {
        main: ".ufaqsw_default_all_faq_container",
        title: ".ufaqsw_default_all_single_faq .ufaqsw_faq_title",
        forPrepend:
          ".ufaqsw_default_all_faq_container .ufaqsw_default_all_faq_content",
        content:
          ".ufaqsw_default_all_faq_container .ufaqsw_default_all_single_faq",
        newTitle: ".vc-sidebar-vaq .ufaqsw_faq_title",
        select: ".vc-sidebar-vaq-select",
      };

      if ($(elFaq.main).length === 0) return;

      function prependSidebar() {
        var title = document.querySelectorAll(elFaq.title);

        var arrTitleHtml = [];
        var arrTitleHtmlSelectOption = [];

        Array.from(title).map((item, index) => {
          var className = item.classList;
          var html = item.innerHTML;
          arrTitleHtml.push(
            `<li class="${className}" id="vc-sidebar-vaq-${index}">${html}</li>`,
          );
          arrTitleHtmlSelectOption.push(
            `<option class="${className}" value="vc-sidebar-vaq-${index}">${html}</option>`,
          );
        });

        var sidebar = `<ul class="vc-sidebar-vaq">
        ${arrTitleHtml.join(" ")}
        </ul>
        `;

        var select = `<select class="vc-sidebar-vaq-select">
        ${arrTitleHtmlSelectOption.join(" ")}
        </select>
        `;

        $(elFaq.forPrepend).prepend(sidebar);
        $(elFaq.forPrepend).prepend(select);
      }

      function hiddenContent() {
        $(elFaq.content).hide();
        $(elFaq.newTitle).first().addClass("active");
        $(`${elFaq.content}`).first().show();
        $(`${elFaq.content}`).first().find(".ufaqsw_container_default").show();
      }

      function handleChangeSelect() {
        $(document).on("change", elFaq.select, function () {
          var value = $(this).val();
          $(`#${value}`).click();
        });
      }

      function handleClick() {
        $(document).on("click", elFaq.newTitle, function () {
          var classId = $(this).attr("class").split(" ")[1];
          var classGetId = classId.split("_");
          var className = classGetId[classGetId.length - 1];

          $(elFaq.newTitle).removeClass("active");
          $(this).addClass("active");

          $(elFaq.content).hide();

          $(`#ufaqsw_single_faq_${className}`).show();
          $(`#ufaqsw_single_faq_${className} .ufaqsw_container_default`).show();
        });
      }

      async function initFaq() {
        await prependSidebar();
        await hiddenContent();
        await handleClick();
        handleChangeSelect();
      }

      initFaq();
    }

    function faqAccordion() {
      const accEl = {
        btn: ".ufaqsw_default_all_single_faq .ufaqsw_toggle_default",
        content: ".ufaqsw_default_all_single_faq .ufaqsw-toggle-inner-default",
      };

      $(document).on("click", accEl.btn, function () {
        var classId = $(this).attr("class").split(" ")[1];
        var classGetId = classId.split("_");
        var className = classGetId[classGetId.length - 1];

        $(accEl.content).hide();
        $(this)
          .find(".ufaqsw-toggle-title-area-default")
          .removeClass("ufaqsw_active");
        $(this)
          .find(`.ufaqsw-toggle-title-area-default_${className}`)
          .addClass("ufaqsw_active");

        $(this)
          .find(
            `.ufaqsw-toggle-inner-default.ufaqsw-toggle-inner-default_${className}`,
          )
          .show();
      });
    }

    function homeSectionThree() {
      var el = {
        tab: ".section-three .nav-link",
        image: ".section-three .tab-pane",
      };

      if ($(el.tab).length == 0) return;

      $(el.image).first().addClass("show");
    }

    function homeSectionFour() {
      var el = {
        tab: ".section-four .nav-link",
        image: ".section-four .tab-pane",
      };

      if ($(el.tab).length == 0) return;
      $(el.image).first().addClass("show");
    }

    // add X to remove field value
    function addXsearch() {
      var el = {
        inputWrapper: ".entry-content-main-faq .ufaqsw_default_all_search",
        input: ".entry-content-main-faq .ufaqsw_default_all_search_box",
        x: ".js-x-search",
      };

      $(el.inputWrapper).append(
        '<span class="vc-x-span js-x-search" style="display: none;">X</span>',
      );

      $(el.input).on("keyup", function () {
        if ($(this).val() == "") {
          $(el.x).css("display", "none");
        } else {
          $(el.x).css("display", "inline");
        }
      });

      $(document).on("click", el.x, function () {
        $(this).css("display", "none");
        $(el.input).val("").keyup();
      });
    }

    init();
  });
})(jQuery);
