<?php

namespace vc;

class Enqueue
{
  public function __construct()
  {
    add_action('wp_enqueue_scripts', [$this, 'vngcd_script_style']);
  }

  public function vngcd_script_style()
  {
    wp_enqueue_style(
      'vngcd-style',
      get_theme_file_uri() . '/vngcd/assets/css/vngcd.css',
      [],
      'auto'
    );

    wp_enqueue_script(
      'vngcd-script',
      get_theme_file_uri() . '/vngcd/assets/js/vngcd.js',
      ['jquery'],
      'auto',
      true
    );
  }
}

new Enqueue;
