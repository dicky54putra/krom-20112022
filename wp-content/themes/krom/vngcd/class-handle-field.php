<?php

namespace vc;

class HandleField
{
  public static function homeSectionFive()
  {
    $ID = get_the_ID();
    $earning = get_field('earnings_comparison', $ID);
    $quick_view = get_field('quick_view', $ID);
    $e_statement = get_field('e_statement', $ID);


    $datas = [
      self::_homeSectionFive($earning, 'earnings'),
      self::_homeSectionFive($quick_view, 'quick_view'),
      self::_homeSectionFive($e_statement, 'e_statement'),
    ];

    return $datas;
  }

  private static function _homeSectionFive($data, $suffix)
  {
    $return = [
      'key' => 'accordion-' . $suffix,
      'title' => $data['title'],
      'icon' => $data['icon_' . $suffix]['url'],
      'image' => $data['image_' . $suffix]['url'],
      'paragraph' => wp_strip_all_tags($data['paragraph']),
    ];

    return $return;
  }
}
