<?php

namespace vc\PostType;

if (!defined('ABSPATH')) {
  exit;
}

class Faq
{
  public function __construct()
  {
    add_action('init', [$this, 'cptui_register_my_cpts']);
  }

  public function cptui_register_my_cpts()
  {
    $labels = [
      "name" => __("Faqs", "vngcd"),
      "singular_name" => __("Faq", "vngcd"),
    ];

    $args = [
      "label" => __("Faqs", "vngcd"),
      "labels" => $labels,
      "description" => "",
      "public" => false,
      "publicly_queryable" => true,
      "show_ui" => true,
      "show_in_rest" => true,
      "rest_base" => "",
      "rest_controller_class" => "WP_REST_Posts_Controller",
      "has_archive" => false,
      "show_in_menu" => true,
      "show_in_nav_menus" => true,
      "delete_with_user" => false,
      "exclude_from_search" => false,
      "capability_type" => "post",
      "map_meta_cap" => true,
      "hierarchical" => false,
      "rewrite" => ["slug" => "faq-new", "with_front" => false],
      "query_var" => true,
      "supports" => ["title"],
      "menu_icon" => "dashicons-format-quote"
    ];

    register_post_type("faq-new", $args);
  }

  public static function getAll()
  {
    $args = [
      'post_type' => 'faq-new',
      'posts_per_page' => -1,
      'post_status' => 'publish',
    ];

    $posts = get_posts($args);

    return self::_restructureData($posts);
  }

  private static function _restructureData($lists)
  {
    $datas = [];
    foreach ($lists as $list) {
      $item = self::_renderItem($list, $list->ID);

      $datas[] = $item;
    }

    return $datas;
  }

  private static function _renderItem($post, $id)
  {
    $questions = [];
    for ($i = 1; $i <= 5; $i++) {
      $question = get_field('question_' . $i, $id);

      $questions[$i] = $question;
    }

    return [
      'id' => $id,
      'title' =>  $post->post_title,
      'faqs' => $questions
    ];
  }
}

new Faq();
