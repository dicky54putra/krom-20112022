<?php

/**
 * Template Name: Faq
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package krom
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

get_header();
?>
<section class="entry-content-main-faq">
    <?php echo do_shortcode("[ufaqsw-all]"); ?>
</section>
<?php get_footer();
