<?php
/**
 * Template Name: Syarat
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package krom
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
?>

<section class="section-pages section-other">
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                <div class="sidebar">
                    <a class="menu-sidebar active" href="#home">Syarat & Ketentuan</a>
                    <a class="menu-sidebar  href="#kebijakan-privasi>Kebijakan Privasi</a>
                </div>
            </div>
            <div class="col-sm-10">
                <div class="content-other">
                        <h1 class="title-other"><?php echo get_field('title'); ?> </h1>
                        <p class="date-other"><?php echo get_field('date'); ?> </p>
                        <p><?php echo get_field('paragraph'); ?></p>
                    </div>
                </div>
        </div>

        <!--faq homepages -->
        <div class="wrap-faq-syarat">
                
            <?php
                if( have_rows('faq') ):
                while( have_rows('faq') ) : the_row();
            ?> 
            <div id="main">
                <div class="container">
                    <div class="accordion" id="faq">
                            <div class="card">
                                <div class="card-header" id="faqhead1">
                                    <a href="#" class="btn btn-header-link" data-toggle="collapse" data-target="#faq1"
                                    aria-expanded="true" aria-controls="faq1">
                                        <?php echo the_sub_field('title_one'); ?>
                                    </a>  
                                </div>

                                <div id="faq1" class="collapse show" aria-labelledby="faqhead1" data-parent="#faq">
                                    <div class="card-body">
                                        <?php echo the_sub_field('paragraph_one'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="faqhead2">
                                    <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq2"
                                    aria-expanded="true" aria-controls="faq2">
                                            <?php echo the_sub_field('title_two'); ?>
                                    </a>
                                </div>

                                <div id="faq2" class="collapse" aria-labelledby="faqhead2" data-parent="#faq">
                                    <div class="card-body">
                                        <?php echo the_sub_field('paragraph_two'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="faqhead3">
                                    <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq3"
                                    aria-expanded="true" aria-controls="faq3">
                                        <?php echo the_sub_field('title_three'); ?>
                                    </a>
                                </div>

                                <div id="faq3" class="collapse" aria-labelledby="faqhead3" data-parent="#faq">
                                    <div class="card-body">
                                            <?php echo the_sub_field('paragraph_three'); ?>
                                    </div>
                                </div>
                            </div>
                                <div class="card">
                                <div class="card-header" id="faqhead4">
                                    <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq4"
                                    aria-expanded="true" aria-controls="faq4">
                                        <?php echo the_sub_field('question_four'); ?>
                                    </a>
                                </div>

                                <div id="faq4" class="collapse" aria-labelledby="faqhead4" data-parent="#faq">
                                    <div class="card-body">
                                            <?php echo the_sub_field('answer_four'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            </div>
            
        </div>
         <?php endwhile; endif; ?>
    </div>
</section>

<?php
get_footer();
?>