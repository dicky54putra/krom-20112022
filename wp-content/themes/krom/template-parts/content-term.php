<?php
/**
 * Template Name: Syarat
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package krom
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
?>


<section>
    <div class="container">

          <div class="row">
            <div class="col-xs-6 col-sm-4 col-lg-2" style="background-color:lightgrey;">
                <div class="sidebar">
                     <?php
                        wp_nav_menu( array(
                            'theme_location' => 'side-bar',
                            'menu_class' => 'side-bar-menu',
                            'container' => ''
                        ) );
                    ?>
                </div>
            </div>
            <div class="col-xs-6 col-sm-8 col-lg-10">
                 <h1><?php echo get_field('title'); ?> </h1>
                 <p><?php echo get_field('date'); ?> </p>
                 <p><?php echo get_field('paragraph'); ?></p>


                <!--faq homepages -->
                <div class="wrap-faq-syarat">
                        
                    <?php
                        if( have_rows('faq') ):
                        while( have_rows('faq') ) : the_row();
                    ?> 
                    <div id="main">
                        <div class="container">
                            <div class="accordion" id="faq">
                                    <div class="card">
                                        <div class="card-header" id="faqhead1">
                                            <a href="#" class="btn btn-header-link" data-toggle="collapse" data-target="#faq1"
                                            aria-expanded="true" aria-controls="faq1">
                                                <?php echo the_sub_field('title_one'); ?>
                                            </a>  
                                        </div>

                                        <div id="faq1" class="collapse show" aria-labelledby="faqhead1" data-parent="#faq">
                                            <div class="card-body">
                                               <?php echo the_sub_field('paragraph_one'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="faqhead2">
                                            <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq2"
                                            aria-expanded="true" aria-controls="faq2">
                                                 <?php echo the_sub_field('title_two'); ?>
                                            </a>
                                        </div>

                                        <div id="faq2" class="collapse" aria-labelledby="faqhead2" data-parent="#faq">
                                            <div class="card-body">
                                                <?php echo the_sub_field('paragraph_two'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="faqhead3">
                                            <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq3"
                                            aria-expanded="true" aria-controls="faq3">
                                                <?php echo the_sub_field('title_three'); ?>
                                            </a>
                                        </div>

                                        <div id="faq3" class="collapse" aria-labelledby="faqhead3" data-parent="#faq">
                                            <div class="card-body">
                                                 <?php echo the_sub_field('paragraph_three'); ?>
                                            </div>
                                        </div>
                                    </div>
                                     <div class="card">
                                        <div class="card-header" id="faqhead4">
                                            <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq4"
                                            aria-expanded="true" aria-controls="faq4">
                                                <?php echo the_sub_field('question_four'); ?>
                                            </a>
                                        </div>

                                        <div id="faq4" class="collapse" aria-labelledby="faqhead4" data-parent="#faq">
                                            <div class="card-body">
                                                 <?php echo the_sub_field('answer_four'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                  </div>
                  
                </div>
                <?php endwhile; endif; ?>

    
    </div>
</section>

<?php
get_footer();
?>