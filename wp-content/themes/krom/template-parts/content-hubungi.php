<?php
/**
 * Template Name: Hubungi Kami
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package krom
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
?>


<section class="section-pages section-hubungi">
  <div class="container">
    <div class="wrap-hubungi">
     <div class="title-hub"><?php the_field('title_hubungi') ?></div>
        <div class="row mb-2">
            <div class="col-md-6">
              <div class="flex-md-row mb-4 h-md-250">
                <div class="d-flex flex-column align-items-start">
                  <p class="card-text mb-auto"><?php the_field('paragraph_hubungi') ?></p>
                </div>
              </div>
            </div>
            <div class="col-md-6">
                <div class="google-map-wrap">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.462606789004!2d106.7988157142474!3d-6.202542162486381!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3e6fb745917%3A0xcaa847399ab06d23!2sDipo%20Tower!5e0!3m2!1sen!2sid!4v1661333188038!5m2!1sen!2sid" width="600" height="400" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
            </div>
      </div>       
    </div>  

     <div class="container">
      <div class="wrap-other">
        <div class="title-hub"><?php the_field('title_keluhan') ?></div>
            <div class="row mb-2">
                <div class="col-md-12">
                  <div class="flex-md-row mb-4 h-md-250">
                    <div class="d-flex flex-column align-items-start">
                      <p class="card-text-hubungi mb-auto"><?php the_field('paragraph_keluhan') ?></p>
                    </div>
                  </div>
                </div>
            </div> 
        </div>
    </div>             
</section>


<?php
get_footer();
