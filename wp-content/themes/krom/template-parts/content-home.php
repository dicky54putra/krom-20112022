<?php

/**
 * Template Name: Home
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package krom
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

get_header();
?>


<?php get_template_part('template-parts/sections/section', 'post') ?>
<?php get_template_part('template-parts/sections/section', 'home-one') ?>
<?php get_template_part('template-parts/sections/section', 'home-two') ?>
<?php get_template_part('template-parts/sections/section', 'home-three') ?>
<?php get_template_part('template-parts/sections/section', 'home-four') ?>
<?php get_template_part('template-parts/sections/section', 'home-five') ?>
<?php get_template_part('template-parts/sections/section', 'home-six') ?>
<?php get_template_part('template-parts/sections/section', 'home-seven') ?>


<?php
get_footer();
