<div class="modal fade" id="download-Modal" tabindex="-1" role="dialog" aria-labelledby="download-Modal-Label" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      <div class="container">
        <div class="row">
          <div class="column">
            <div class="card-body-download d-flex flex-column align-items-start">
              <div class="title-download">Butuh yang flexible dan </br> gak ribet ?</div>
              <div class="prg-download">Yuk, aktivasi Krom dan nikmati fleksibilitas bank </br> digital masa kini! </div>
              <a href="#">
                <img src="<?= get_site_url() ?>/wp-content/uploads/2022/08/play-store.png" alt="google-play">
              </a>
              <div class="prg-download">Scan kode QR di bawah ini untuk mendownload </br> aplikasi Krom.</div>
              <a href="#">
                <img src="<?= get_site_url() ?>/wp-content/uploads/2022/08/barcode.png" alt="barcode">
              </a>
            </div>
          </div>
          <div class="column">
            <div class="purple">
              <img src="<?= get_site_url() ?>/wp-content/uploads/2022/09/img_download.png" alt="Download">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>