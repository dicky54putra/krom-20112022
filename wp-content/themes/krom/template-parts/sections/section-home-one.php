<!--Section one -->
<?php
$heading_one = get_field('heading_one');
$sub_one = get_field('sub_one');
$button_one = get_field('button_one');
$images_one = get_field('images_one');
?>
<section class="section-pages bg-homepages">
  <div class="container">
    <div class="row featurette homepages">
      <div class="col-md-4 order-md-1">
        <img class="bd-placeholder-img bd-placeholder-img-lg featurette-image img-fluid mx-auto" src="<?php echo $images_one['url']; ?>" alt="<?php echo $images_one['title']; ?>" />
      </div>
      <div class="col-md-8 order-md-2">
        <?php if ($heading_one) { ?>
          <h1 class="hero-title"><?php echo $heading_one; ?></h1>
        <?php } ?>
        <?php if ($sub_one) { ?>
          <p class="hero-prg">
            <?php echo $sub_one; ?>
          </p>
        <?php } ?>
        <?php if (isset($button_one['text']) && !empty($button_one['text'])) { ?>
          <a href="<?php echo $button_one['url']; ?>" class="btn btn-primary btn-download button-modal" data-toggle="modal" data-target="#download-Modal" <?php echo ($button_one['open_in_new_tab'] ? ' target="_blank"' : ''); ?>><?php echo $button_one['text']; ?></a>
          <a href="<?php echo $button_one['url']; ?>" class="btn btn-primary btn-download button-link" <?php echo ($button_one['open_in_new_tab'] ? ' target="_blank"' : ''); ?>><?php echo $button_one['text']; ?></a>
        <?php } ?>
      </div>
    </div>
  </div>
</section>