<!--Section six -->
<?php
$image = get_field('image');
?>
<section class="section-pages section-six">
  <div class="container">
    <div class="row featurette featurette-banner">
      <div class="col-md-8 order-md-2">
        <h2 class="banner_title"><?php echo get_field('banner_title'); ?></h2>
        <p class="banner_paragraph"><?php echo get_field('banner_paragraph'); ?></p>
      </div>
      <div class="col-md-4 order-md-1">
        <div class="container">
          <img class="banner-img" src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>" />
        </div>
      </div>
    </div>
  </div>
</section>