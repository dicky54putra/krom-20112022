<!--Section five tabs-->
<section class="section-pages section-five">
  <div class="container">
    <div class="accordion" id="accordionMain">
      <?php
      if (have_rows('earnings_comparison')) :
        while (have_rows('earnings_comparison')) : the_row();
          $icon_earnings = get_sub_field('icon_earnings');
          $image_earnings = get_sub_field('image_earnings');
      ?>
          <div class="card">
            <div class="card-header" id="headingOne">
              <div class="row">
                <div class="col"></div>
                <div class="col">
                  <h5 class="mb-0">
                    <button class="btn btn-link-one" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      <h1 class="earning"><?php the_sub_field('title') ?></h1>
                    </button>
                  </h5>
                </div>
              </div>
            </div>
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionMain">
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <div class="wrap-images-one">
                      <?php if ($image_earnings) { ?>
                        <div class="earning-images-one">
                          <img src="<?php echo $image_earnings['url']; ?>" alt="<?php echo $title; ?>" />
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                  <div class="col">
                    <div class="wrap-tab-fitur">
                      <?php if ($icon_earnings) { ?>
                        <div class="earning-icon">
                          <img src="<?php echo $icon_earnings['url']; ?>" alt="<?php echo $title; ?>" />
                        </div>
                      <?php } ?>
                      <div class="p-earning"><?php the_sub_field('paragraph') ?></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php endwhile; ?>
      <?php endif; ?>


      <?php
      if (have_rows('quick_view')) :
        while (have_rows('quick_view')) : the_row();
          $icon_quick_view = get_sub_field('icon_quick_view');
          $image_quick_view = get_sub_field('image_quick_view');
      ?>
          <div class="card">
            <div class="card-header" id="headingTwo">
              <div class="row">
                <div class="col"></div>
                <div class="col">
                  <h5 class="mb-0">

                    <button class="btn btn-link-two" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                      <h1 class="earning"><?php the_sub_field('title') ?></h1>
                    </button>
                  </h5>
                </div>
              </div>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionMain">
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <?php if ($image_quick_view) { ?>
                      <div class="earning-images-two">
                        <img src="<?php echo $image_quick_view['url']; ?>" alt="<?php echo $title; ?>" />
                      </div>
                    <?php } ?>
                  </div>
                  <div class="col">
                    <div class="wrap-tab-fitur">
                      <?php if ($icon_quick_view) { ?>
                        <div class="earning-icon">
                          <img src="<?php echo $icon_quick_view['url']; ?>" alt="<?php echo $title; ?>" />
                        </div>
                      <?php } ?>
                      <div class="p-earning"><?php the_sub_field('paragraph') ?></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      <?php endwhile;
      endif; ?>

      <?php
      if (have_rows('e_statement')) :
        while (have_rows('e_statement')) : the_row();
          $icon_e_statement = get_sub_field('icon_e_statement');
          $image_e_statement = get_sub_field('image_e_statement');
      ?>
          <div class="card">
            <div class="card-header" id="headingThree">
              <div class="row">
                <div class="col"></div>
                <div class="col">
                  <h5 class="mb-0">
                    <button class="btn btn-link-three" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                      <h1 class="earning"><?php the_sub_field('title') ?></h1>
                    </button>
                  </h5>
                </div>
              </div>
            </div>
            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionMain">
              <div class="card-body">
                <div class="row">
                  <div class="col">
                    <?php if ($image_quick_view) { ?>
                      <div class="earning-images-threee">
                        <img src="<?php echo $image_e_statement['url']; ?>" alt="<?php echo $title; ?>" />
                      </div>
                    <?php } ?>
                  </div>
                  <div class="col">
                    <div class="wrap-tab-fitur">
                      <?php if ($icon_e_statement) { ?>
                        <div class="earning-icon">
                          <img src="<?php echo $icon_e_statement['url']; ?>" alt="<?php echo $title; ?>" />
                        </div>
                      <?php } ?>
                      <div class="p-earning"><?php the_sub_field('paragraph') ?></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      <?php endwhile;
      endif; ?>
    </div>
  </div>
</section>