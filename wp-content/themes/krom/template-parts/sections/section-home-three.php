<!--Section three -->
<section id="layanan" class="section-pages section-three">
  <div class="container">
    <div class="text-center mb-4">
      <h1 class="title_main"><?php echo get_field('title_main'); ?></h1>
      <p class="paragraph_main"><?php echo get_field('paragraph_main'); ?></p>
    </div>

    <div class="wrap-tab">
      <div class="row wrap-layout-tab">
        <!-- Tab panes -->
          <div class="layout-tab-feature">
            <ul class="nav nav-tabs" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" href="#feature_utama" role="tab" data-toggle="tab">
                  <?php
                  if (have_rows('feature_utama')) :
                    while (have_rows('feature_utama')) : the_row();
                  ?>
                      <?php the_sub_field('title_utama') ?>
                  <?php endwhile;
                  endif; ?>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#feature_tabungan" role="tab" data-toggle="tab">
                  <?php
                  if (have_rows('feature_tabungan')) :
                    while (have_rows('feature_tabungan')) : the_row();
                      $image_tabungan = get_sub_field('image_tabungan');
                  ?>
                      <?php the_sub_field('title_tabungan') ?>
                  <?php endwhile;
                  endif; ?>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#references" role="tab" data-toggle="tab">
                  <?php
                  if (have_rows('feature_Deposito')) :
                    while (have_rows('feature_Deposito')) : the_row();
                      $image_deposito = get_sub_field('image_deposito');
                  ?>
                      <?php the_sub_field('title_deposito') ?>
                  <?php endwhile;
                  endif; ?>
                </a>
              </li>
            </ul>
          </div>
        <div class="col-lg-12">
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="feature_utama">
              <?php
              if (have_rows('feature_utama')) :
                while (have_rows('feature_utama')) : the_row();
                  $images_utama = get_sub_field('images_utama');
              ?>
                  <div class="row">
                    <div class="col-md-5">
                      <div class="wrap-images-three">
                        <?php if ($images_utama) { ?>
                          <div class="vc-layanan-image layanan-image">
                            <img src="<?php echo $images_utama['url']; ?>" alt="<?php echo $title; ?>" />
                          </div>
                        <?php } ?>

                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="layanan-prg">
                        <?php the_sub_field('paragraph_utama') ?>
                      </div>
                    </div>
                  </div>
              <?php endwhile;
              endif; ?>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="feature_tabungan">
              <?php
              if (have_rows('feature_tabungan')) :
                while (have_rows('feature_tabungan')) : the_row();
                  $image_tabungan = get_sub_field('image_tabungan');
              ?>
                  <div class="row">
                    <div class="col-md-5">
                      <div class="wrap-images-three">
                        <?php if ($image_tabungan) { ?>
                          <div class="vc-layanan-image layanan-image">
                            <img src="<?php echo $image_tabungan['url']; ?>" alt="<?php echo $title; ?>" />
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                    <div class="col-md-7">
                        <div class="layanan-prg">
                          <?php the_sub_field('paragraph_tabungan') ?>
                        </div>
                    </div>
                  </div>
              <?php endwhile;
              endif; ?>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="references">
              <?php
              if (have_rows('feature_Deposito')) :
                while (have_rows('feature_Deposito')) : the_row();
                  $image_deposito = get_sub_field('image_deposito');
              ?>
                  <div class="row">
                    <div class="col-md-5">
                      <?php if ($image_deposito) { ?>
                        <div class="vc-layanan-image layanan-image">
                          <img src="<?php echo $image_deposito['url']; ?>" alt="<?php echo $title; ?>" />
                        </div>
                      <?php } ?>
                    </div>
                    <div class="col-md-7">
                      <div class="layanan-prg">
                          <?php the_sub_field('paragraph_deposito') ?>
                      </div>
                    </div>
                  </div>
              <?php endwhile;
              endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>