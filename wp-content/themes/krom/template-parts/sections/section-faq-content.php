<?php

use vc\PostType\Faq;

$faqs = Faq::getAll();
?>

<seaction class="vc-faq-content">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="vc-faqs">
          <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <?php foreach ($faqs as $key => $faq) { ?>
              <a class="nav-link <?= $key === 0 ? 'active' : '' ?>" id="v-tab<?= $faq['id'] ?>" data-toggle="pill" href="#<?= 'faq-' . $faq['id'] ?>" role="tab" aria-controls="<?= 'faq-' . $faq['id'] ?>" aria-selected="true"><?= $faq['title'] ?></a>
            <?php } ?>
          </div>
          <div class="tab-content" id="v-pills-tabContent">
            <?php foreach ($faqs as $key => $faq) { ?>
              <div class="tab-pane fade <?= $key === 0 ? 'show active' : '' ?>" id="<?= 'faq-' . $faq['id'] ?>" role="tabpanel" aria-labelledby="v-tab<?= $faq['id'] ?>">
                <div id="accordion-faq" class="js-accordion-faq vc-accordion-lists">

                  <?php foreach ($faq['faqs'] as $keyChild => $data) { ?>
                    <div class="vc-cardfaq js-vc-cardfaq">
                      <div class="vc-cardfaq-header js-btn-acc <?= $keyChild == 0 ? 'hide' : '' ?>" id="<?= $faq['id'] . '-' . $keyChild . '-Heading' ?>">
                        <h5 class="mb-0" data-toggle="collapse" data-target="#<?= $faq['id'] . '-' . $keyChild ?>" aria-expanded="true" aria-controls="<?= $faq['id'] . '-' . $keyChild ?>">
                          <?= $data['question'] ?>
                        </h5>
                      </div>

                      <div id="<?= $faq['id'] . '-' . $keyChild ?>" class="collapse <?= $keyChild == 0 ? 'show' : '' ?>" aria-labelledby="<?= $faq['id'] . '-' . $keyChild . '-Heading' ?>" data-parent="#accordion-home-section-five">
                        <div class="vc-cardfaq-body">
                          <?= $data['answer'] ?>
                        </div>
                      </div>
                    </div>
                  <?php } ?>

                </div>
              </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</seaction>