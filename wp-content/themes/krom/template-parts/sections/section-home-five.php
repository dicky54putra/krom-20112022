<?php

use vc\HandleField;

$datas = HandleField::homeSectionFive();

?>

<!--Section five tabs-->
<section class="section-pages section-five">
  <div class="container">
    <div class="text-center mb-4">
        <h1 class="title_main"><?php echo get_field('feature'); ?></h1>
        <p class="paragraph_main"><?php echo get_field('sub_feature'); ?></p>
      </div>
    <div class="row">
      <div class="col-12 col-lg-4">
        <img class="js-home-section-five-image" src="<?= $datas[0]['image'] ?>" alt="<?= $datas[0]['title'] ?>">
      </div>
      <div class="col-12 col-lg-8">
        <div id="accordion-home-section-five" class="js-accordion-home-section-five vc-accordion-lists">
          <?php foreach ($datas as $key => $data) { ?>
            <div class="vc-card js-vc-card">
              <div class="vc-card-header js-btn-acc <?= $key == 0 ? 'hide' : '' ?>" id="<?= $data['key'] . '-Heading' ?>" data-img="<?= $data['image'] ?>">
                <h5 class="mb-0" data-toggle="collapse" data-target="#<?= $data['key'] ?>" aria-expanded="true" aria-controls="<?= $data['key'] ?>">
                  <?= $data['title'] ?>
                </h5>
              </div>

              <div id="<?= $data['key'] ?>" class="collapse <?= $key == 0 ? 'show' : '' ?>" aria-labelledby="<?= $data['key'] . '-Heading' ?>" data-parent="#accordion-home-section-five">
                <div class="vc-card-body">
                  <img class="vc-card-header__icon" height="20" src=" <?= $data['icon'] ?>" alt="<?= $data['title'] ?>">
                  <h5><?= $data['title'] ?></h5>
                  <div class="v-card-content"><?= $data['paragraph'] ?></div>
                </div>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</section>