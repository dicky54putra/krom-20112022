<section class="section-posts">
  <div class="container">
    <?php if (have_posts()) {
      while (have_posts()) : the_post(); ?><br>
        <div class="list-posting">
          <p><?php the_content(__('(more...)')); ?></p>
        </div>
      <?php
      endwhile; ?>
    <?php } ?>
  </div>
</section>