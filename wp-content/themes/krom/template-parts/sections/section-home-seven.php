<!--Section seven faq -->
<section class="section-pages section-seven">
  <div class="container mt-3">
    <div class="text-center mb-4">
      <div class="title_main"><?php echo get_field('title_faq'); ?> </div>
      <p><?php echo get_field('subtitle_faq'); ?></p>
    </div>

    <!--faq homepages -->
    <div class="wrap-faq-home vc-faq-home">

      <?php
      if (have_rows('faq_home')) :
        while (have_rows('faq_home')) : the_row();
      ?>
          <div id="main">
            <div class="container">
              <div class="accordion" id="faq">
                <div class="card-faq">
                  <div class="card-header-faq" id="faqhead1">
                    <a href="#" class="btn btn-header-link" data-toggle="collapse" data-target="#faq1" aria-expanded="true" aria-controls="faq1">
                      <?php echo the_sub_field('question_one'); ?>
                    </a>
                  </div>

                  <div id="faq1" class="collapse show" aria-labelledby="faqhead1" data-parent="#faq">
                    <div class="card-body-faq">
                      <?php echo the_sub_field('answer_one'); ?>
                    </div>
                  </div>
                </div>
                <div class="card-faq">
                  <div class="card-header-faq" id="faqhead2">
                    <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq2" aria-expanded="true" aria-controls="faq2">
                      <?php echo the_sub_field('question_two'); ?>
                    </a>
                  </div>

                  <div id="faq2" class="collapse" aria-labelledby="faqhead2" data-parent="#faq">
                    <div class="card-body-faq">
                      <?php echo the_sub_field('answer_two'); ?>
                    </div>
                  </div>
                </div>
                <div class="card-faq">
                  <div class="card-header-faq" id="faqhead3">
                    <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq3" aria-expanded="true" aria-controls="faq3">
                      <?php echo the_sub_field('question_three'); ?>
                    </a>
                  </div>

                  <div id="faq3" class="collapse" aria-labelledby="faqhead3" data-parent="#faq">
                    <div class="card-body-faq">
                      <?php echo the_sub_field('answer_three'); ?>
                    </div>
                  </div>
                </div>
                <div class="card-faq">
                  <div class="card-header-faq" id="faqhead4">
                    <a href="#" class="btn btn-header-link collapsed" data-toggle="collapse" data-target="#faq4" aria-expanded="true" aria-controls="faq4">
                      <?php echo the_sub_field('question_four'); ?>
                    </a>
                  </div>

                  <div id="faq4" class="collapse" aria-labelledby="faqhead4" data-parent="#faq">
                    <div class="card-body-faq">
                      <?php echo the_sub_field('answer_four'); ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      <?php endwhile;
      endif; ?>
    </div>
    <div class="col-12 d-flex justify-content-center align-items-center">
      <a class="btn btn-lainnya" href="faq">Lainnya</a>
    </div>

</section>