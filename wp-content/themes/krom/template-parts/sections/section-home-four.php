<!--Section four tabs -->
<section class="section-pages section-four">
  <div class="container mt-3">
    <div class="row">
      <div class="col-12">
        <ul class="nav nav-pills">
          <li class="nav-item">
            <?php
            if (have_rows('transfer')) :
              while (have_rows('transfer')) : the_row();
            ?>
                <a class="nav-link active" data-toggle="pill" href="#transfer" role="tab" aria-controls="pills-transfer" aria-selected="true"><?php the_sub_field('title') ?></a>
            <?php endwhile;
            endif; ?>
          </li>

          <li class="nav-item">
            <?php
            if (have_rows('top_up')) :
              while (have_rows('top_up')) : the_row();
            ?>
                <a class="nav-link" data-toggle="pill" href="#cuckoo" role="tab" aria-controls="pills-cuckoo" aria-selected="false"><?php the_sub_field('title') ?></a>
            <?php endwhile;
            endif; ?>
          </li>

        </ul>
        <div class="tab-content mt-3">
          <div class="tab-pane fade show active" id="transfer" role="tabpanel" aria-labelledby="transfer-tab">
            <?php
            if (have_rows('transfer')) :
              while (have_rows('transfer')) : the_row();
                $image_transfer = get_sub_field('image_transfer');
            ?>
                <div class="row">
                  <div class="col-md-7">
                    <?php the_sub_field('paragraph') ?>
                  </div>
                  <div class="col-md-4">
                    <?php if ($image_transfer) { ?>
                      <div class="vc-image-trans image-trans">
                        <img src="<?php echo $image_transfer['url']; ?>" alt="<?php echo $title; ?>" />
                      </div>
                    <?php } ?>
                  </div>
                </div>
            <?php endwhile;
            endif; ?>
          </div>

          <div class="tab-pane fade" id="cuckoo" role="tabpanel" aria-labelledby="feature_utama-tab">
            <?php
            if (have_rows('top_up')) :
              while (have_rows('top_up')) : the_row();
                $image_topup = get_sub_field('image_topup');
            ?>
                <div class="row">
                  <div class="col-md-6">
                    <?php the_sub_field('paragraph') ?>
                  </div>
                  <div class="col-md-6">
                    <?php if ($image_topup) { ?>
                      <div class="vc-image-trans image-trans">
                        <img src="<?php echo $image_topup['url']; ?>" alt="<?php echo $title; ?>" />
                      </div>
                    <?php } ?>
                  </div>
                </div>
            <?php endwhile;
            endif; ?>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>