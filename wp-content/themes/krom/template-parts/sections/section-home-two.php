<!--Section two -->
<?php
$title_two = get_field('title_two');
$prg_two = get_field('prg_two');
$button_two = get_field('button_two');
$images_two = get_field('images_two');
?>
<section class="section-pages section-two">
  <div class="container h-100 p-3">
    <div class="row">
      <div class="col-md-8">
        <?php if ($title_two) { ?>
          <div class="title-two"><?php echo $title_two ?></div>
        <?php } ?>
        <?php if ($prg_two) { ?>
          <div class="prg-two">
            <?php echo $prg_two; ?>
          </div>
        <?php } ?>
        <!--<?php if (isset($button_two['text']) && !empty($button_two['text'])) { ?>
          <a href="<?php echo $button_two['url']; ?>" class="btn-pelajari" <?php echo ($button_two['open_in_new_tab'] ? ' target="_blank"' : ''); ?>><?php echo $button_two['text']; ?></a>
        <?php } ?>-->
      </div>
      <div class="col-md-4">
        <img class="bd-placeholder-img featurette-image-kebutuhan img-fluid mx-auto" src="<?php echo $images_two['url']; ?>" alt="<?php echo $images_two['title']; ?>" />
      </div>
    </div>

    <div class="container">
      <div class="row g-4 row-cols-1 col-sm-12 feature-wrapper">
        <div class="feature col">
          <div class="feature-icon">
            <?php
            if (have_rows('feature-one')) :
              while (have_rows('feature-one')) : the_row();
                $feat_images_sub_one = get_sub_field('feat_images_sub_one');
            ?>
                <?php if ($feat_images_sub_one) { ?>
                  <div class="feature-image">
                    <img src="<?php echo $feat_images_sub_one['url']; ?>" alt="<?php echo $title; ?>" />
                  </div>
                <?php } ?>
                <h2 class="feature-two"><?php the_sub_field('feat_title_sub_one') ?></h2>
                <p class="feature-icon"><?php the_sub_field('feat_prg_sub_one') ?></p>
            <?php endwhile;
            endif; ?>
          </div>
        </div>

        <div class="feature col">
          <div class="feature-icon">
            <?php
            if (have_rows('feature-two')) :
              while (have_rows('feature-two')) : the_row();
                $feat_images_sub_two = get_sub_field('feat_images_sub_two');
            ?>
                <?php if ($feat_images_sub_two) { ?>
                  <div class="feature-image">
                    <img src="<?php echo $feat_images_sub_two['url']; ?>" alt="<?php echo $title; ?>" />
                  </div>
                <?php } ?>
                <h2 class="feature-two"><?php the_sub_field('feat_title_sub_two') ?></h2>
                <p class="feature-icon"><?php the_sub_field('feat_prg_sub_two') ?></p>
            <?php endwhile;
            endif; ?>

          </div>
        </div>

        <div class="feature col">
          <div class="feature-icon">
            <?php
            if (have_rows('feature-three')) :
              while (have_rows('feature-three')) : the_row();
                $feat_images_sub_three = get_sub_field('feat_images_sub_three');
            ?>
                <?php if ($feat_images_sub_three) { ?>
                  <div class="feature-image">
                    <img src="<?php echo $feat_images_sub_three['url']; ?>" alt="<?php echo $title; ?>" />
                  </div>
                <?php } ?>
                <h2 class="feature-two"><?php the_sub_field('feat_title_sub_three') ?></h2>
                <p class="feature-icon"><?php the_sub_field('feat_prg_sub_three') ?></p>
            <?php endwhile;
            endif; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>