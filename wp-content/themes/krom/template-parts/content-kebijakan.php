<?php
/**
 * Template Name: Kebijakan
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package krom
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
?>

<section class="section-pages section-other">
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                <div class="sidebar">
                    <a class="menu-sidebar href="#home>Syarat & Ketentuan</a>
                    <a class="menu-sidebar active">Kebijakan Privasi</a>
                </div>
            </div>
            <div class="col-sm-10">
                <div class="content-other">
                        <h1 class="title-other"><?php echo get_field('title'); ?> </h1>
                        <p class="date-other"><?php echo get_field('date'); ?> </p>
                        <p><?php echo get_field('paragraph'); ?></p>
                    </div>
                </div>
        </div>

    </div>
</section>




<?php
get_footer();
?>
