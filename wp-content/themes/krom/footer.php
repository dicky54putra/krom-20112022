<?php

/**
 * The template for displaying the footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package krom
 */

?>

<!-- Modal download -->
<?php get_template_part('template-parts/sections/section', 'utils') ?>
<footer class="mainfooter" role="contentinfo">
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <img src="<?= get_site_url() ?>/wp-content/uploads/2022/08/ic_krom_logo.png" width="115px" height="32px">
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="footer-pad">
                        <?php
                        wp_nav_menu(array(
                            'theme_location' => 'menu-footer',
                            'menu_class' => 'footer-menu',
                            'container' => ''
                        ));
                        ?>
                    </div>
                </div>
            </div>
            <div class="wrap-logo-lps">
                <div class="row">
                    <div class="col-md-1">
                        <img src="<?= get_site_url() ?>/wp-content/uploads/2022/08/image-43.png">
                    </div>
                    <div class="col-md-11">
                        <p class="textleft">Krom terdaftar dan diawasi oleh Otoritas Jasa Keuangan (OJK) dan merupakan peserta program penjaminan Lembaga Penjamin Simpanan (LPS).</p>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center copyright">&copy; 2022 Krom. All rights reserved. version 0.0.1</div>
                </div>
            </div>


        </div>
    </div>
</footer>


<?php wp_footer(); ?>

</body>

</html>